package com.sda.up.amen.dp.strategy;

public class Main {

	public static void main(String[] args) {
		Hero h = new Hero("Władek", 100, 100);
		h.setStrategy(new StrategyArcher());
		h.fightDragon();
	}

}
