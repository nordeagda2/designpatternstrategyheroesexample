package com.sda.up.amen.dp.strategy;

public interface IStrategy {
	void fight();
}
